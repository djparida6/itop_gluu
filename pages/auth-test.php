<?php 
// 
// Script to test External Authentication 
// 
require_once('../approot.inc.php'); 
require_once(APPROOT.'core/config.class.inc.php'); 
require_once(APPROOT.'application/utils.inc.php'); 
 
echo "<h1>External Authentication: Integration Test Script</h1>"; 
try 
{ 
        $aAllowedLoginTypes = utils::GetConfig()->GetAllowedLoginTypes(); 
        $sAllowedLoginTypes = implode('|', $aAllowedLoginTypes); 
        echo "<p>allowed_login_types set to '$sAllowedLoginTypes'</p>\n"; 
 
        if (!in_array('external', $aAllowedLoginTypes)) 
        { 
                    echo "<p>Check your iTop configuration file, ". 
                           "<b>allowed_login_type</b> does not enable ". 
                           "'external' authentication.</p>"; 
        } 
        else 
        { 
                   echo "<p>Ok, external authentication is enabled by allowed_login_type.</p>"; 
                $sExternalAuthVariable = utils::GetConfig()->GetExternalAuthenticationVariable(); 
                echo "<p>External Authentication Variable: '$sExternalAuthVariable'</p>\n"; 
                if ($sExternalAuthVariable != '') 
                { 
 
                        $sEval = '$bVarIsSet = isset('.$sExternalAuthVariable.');'; 
                        eval($sEval); 
                        if ($bVarIsSet) 
                        { 
                                $sEval = '$sAuthUser = '.$sExternalAuthVariable.';'; 
                                eval($sEval); 
                                echo "<p>External Authentication Variable set to: ". 
                                         "'$sAuthUser'</p>\n"; 
                                if ($sAuthUser != '') 
                                { 
                                        echo "<p><b>Ok, integration with external authentication ". 
                                                 "successful !</b></p>\n"; 
                                } 
                                else 
                                { 
                                        echo "<p>Empty user information passed by the web ". 
                                                 "server.</p>\n"; 
                                } 
                        } 
                        else 
                        { 
                                echo "<p>External Authentication Variable <b>NOT</b> set.</p>\n"; 
                        } 
                } 
                else 
                { 
                        echo "<p>Check your iTop configuration file, <b>ext_auth_var</b> is set ". 
                                 "to an empty string.</p>\n"; 
                } 
        } 
} 
catch(ConfigException $e) 
{ 
        echo "<p>iTop configuration file not found. Did you already install iTop ?</p>\n"; 
} 
echo "<hr/>\n"; 
echo "<p>For information:</p>"; 
echo "<pre>\$_SERVER variable:\n"; 
print_r($_SERVER); 
echo "\$_COOKIE variable:"; 
print_r($_COOKIE); 
echo "</pre>\n"; 
?>