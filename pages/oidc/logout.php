<?php

include_once 'OIDC.php';

$oidc = new OIDC();

$end_session_EP = $oidc->getEndSessionEndpoint();
$post_logout_redirect_url = $oidc->getPostLogoutRedirectURL();

$url = "$end_session_EP?post_logout_redirect_uri=$post_logout_redirect_url";

return $url;


?>