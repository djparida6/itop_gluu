<?php
$oP->DisableBreadCrumb();
$sClass = utils::ReadPostedParam('class', '', 'class');
$sClassLabel = MetaModel::GetName($sClass);
    $sTransactionId = utils::ReadPostedParam('transaction_id', '', 'transaction_id');
$aErrors = array();
if ( empty($sClass) ) // TO DO: check that the class name is valid !
{
    throw new ApplicationException(Dict::Format('UI:Error:1ParametersMissing', 'class'));
}
if (!utils::IsTransactionValid($sTransactionId, false))
{
    $sUser = UserRights::GetUser();
    IssueLog::Error("UI.php '$operation' : invalid transaction_id ! data: user='$sUser', class='$sClass'");
    $oP->p("<strong>".Dict::S('UI:Error:ObjectAlreadyCreated')."</strong>\n");
}
else
{
    /** @var \cmdbAbstractObject $oObj */
    $oObj = MetaModel::NewObject($sClass);
    $sStateAttCode = MetaModel::GetStateAttributeCode($sClass);
    if (!empty($sStateAttCode))
    {
        $sTargetState = utils::ReadPostedParam('obj_state', '');
        if ($sTargetState != '')
        {
            $oObj->Set($sStateAttCode, $sTargetState);
        }
    }
    $aErrors = $oObj->UpdateObjectFromPostedForm();
}
if (isset($oObj) && is_object($oObj))
{
    $sClass = get_class($oObj);
    $sClassLabel = MetaModel::GetName($sClass);

    try
    {
        if (!empty($aErrors))
        {
            throw new CoreCannotSaveObjectException(array('id' => $oObj->GetKey(), 'class' => $sClass, 'issues' => $aErrors));
        }

        $oObj->DBInsertNoReload();// No need to reload

        utils::RemoveTransaction($sTransactionId);
        $oP->set_title(Dict::S('UI:PageTitle:ObjectCreated'));

        // Compute the name, by reloading the object, even if it disappeared from the silo
        $oObj = MetaModel::GetObject($sClass, $oObj->GetKey(), true /* Must be found */, true /* Allow All Data*/);
        $sName = $oObj->GetName();
        $sMessage = Dict::Format('UI:Title:Object_Of_Class_Created', $sName, $sClassLabel);

        $sNextAction = utils::ReadPostedParam('next_action', '');
        if (!empty($sNextAction))
        {
            $oP->add("<h1>$sMessage</h1>");
            ApplyNextAction($oP, $oObj, $sNextAction);
        }
        else
        {
            // Nothing more to do
            ReloadAndDisplay($oP, $oObj, 'create', $sMessage, 'ok');
        }
    }
    catch (CoreCannotSaveObjectException $e)
    {
        // Found issues, explain and give the user a second chance
        //
        $aIssues = $e->getIssues();

        $oP->set_title(Dict::Format('UI:CreationPageTitle_Class', $sClassLabel));
        $oP->add("<h1>".MetaModel::GetClassIcon($sClass)."&nbsp;".Dict::Format('UI:CreationTitle_Class', $sClassLabel)."</h1>\n");
        $oP->add("<div class=\"wizContainer\">\n");
        $oP->AddHeaderMessage($e->getHtmlMessage(), 'message_error');
        cmdbAbstractObject::DisplayCreationForm($oP, $sClass, $oObj);
        $oP->add("</div>\n");
    }
}
break;
?>